//
//  ContentView.swift
//  Dummy_Project
//
//  Created by Tanvir Alam on 8/9/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, iOS 13")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
